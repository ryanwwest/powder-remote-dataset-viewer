#!/usr/bin/env python

import os

import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.igext as ig
import geni.rspec.emulab.pnext as PN


tourDescription = """
# Connect to a remote dataset that you specify in parameters.

See: https://docs.powderwireless.net/advanced-storage.html.

"""

tourInstructions = """

"""

BIN_PATH = "/local/repository/bin"
ETC_PATH = "/local/repository/etc"
UBUNTU_JAMMY_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD"
COMP_MANAGER_ID = "urn:publicid:IDN+emulab.net+authority+cm"

pc = portal.Context()

node_types = [
    ("d430", "Emulab, d430"),
    ("d740", "Emulab, d740"),
]
pc.defineParameter(
    name="node_type",
    description="Type of compute node to use",
    typ=portal.ParameterType.STRING,
    defaultValue=node_types[0],
    legalValues=node_types
)

pc.defineParameter(
    name="remote_data_source",
    description="remote data source that must be specified for this profile to be useful",
    typ=portal.ParameterType.STRING,
    defaultValue=""
)

params = pc.bindParameters()
pc.verifyParameters()
request = pc.makeRequestRSpec()

server = request.RawPC("server")
server.component_manager_id = COMP_MANAGER_ID
server.hardware_type = params.node_type
server.disk_image = UBUNTU_JAMMY_IMG
if params.remote_data_source:
    iface = server.addInterface()
    fsnode = request.RemoteBlockstore("fsnode", "/data")
    fsnode.dataset = params.remote_data_source
    fsnode.rwclone = True
    fsnode.readonly = True
    fslink = request.Link("fslink")
    fslink.addInterface(iface)
    fslink.addInterface(fsnode.interface)
    fslink.best_effort = True
    fslink.vlan_tagging = True

tour = ig.Tour()
tour.Description(ig.Tour.MARKDOWN, tourDescription)
tour.Instructions(ig.Tour.MARKDOWN, tourInstructions)
request.addTour(tour)

pc.printRequestRSpec(request)
